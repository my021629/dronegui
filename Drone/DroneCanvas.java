/**
 * all the information realted to canvas size and images drawn on canvas will be stored in this class.
 */

import javafx.application.Application;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class DroneCanvas extends Application {
    int canvasWidth;
    int canvasHeight;
    GraphicsContext gc;
    final Image droneImage = new Image("Drone.png"); // image of drone to be shown on canvas

    /*
     * constructor for canvas
     */

    public DroneCanvas(GraphicsContext graph, int canvasWidth, int canvasHeight) {
        gc = graph;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
    }

    public void setFillArenaColour(int CanWid, int CanHei) {
        gc.setFill(Color.YELLOW);
        gc.fillRect(0, 0, CanWid, CanWid);
        gc.setStroke(Color.YELLOW);
        gc.strokeRect(0, 0, CanWid, CanWid);
    }
    public void changeCanvas(DroneArena Arena){
        gc.clearRect(0, 0, canvasWidth, canvasHeight);
        setFillArenaColour(canvasWidth, canvasHeight);
        drawDrone(Arena);
    }

    /**
     * this fucntion will go through the drones array list using enhanced for loop and draw every drone on canvas.
     * @param Arena
     */
    public void drawDrone(DroneArena Arena) { 
        for (Drone d : Arena.droneslist) {
            gc.drawImage(droneImage, d.getX(), d.getY(), 45, 45);  // going through the arraylist and drawing
        }

    }

    @Override
    public void start(Stage PrimaryStage) throws Exception {
    }
}