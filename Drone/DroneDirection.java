import java.util.Random;

public class DroneDirection {

    public enum avalibleDirections {
        NORTH,
        NORTHNORTHEAST,
        NORTHEAST,
        EASTNORTHEAST,
        EAST,
        EASTSOUTHEAST,
        SOUTHEAST,
        SOUTHSOUTHEAST,
        SOUTH,
        SOUTHSOUTHWEST,
        SOUTHWEST,
        WESTSOUTHWEST,
        WEST,
        NORTHWESTNORTH,
        NORTHWEST,
        NORTHNORTHWEST;

        public static avalibleDirections randomDirections() {
            Random rand = new Random();
            return values()[rand.nextInt(values().length)];
        }

        public avalibleDirections newDirection(avalibleDirections dir) {
            if (dir == avalibleDirections.NORTH)
                return avalibleDirections.NORTHNORTHEAST;
            else if (dir == avalibleDirections.NORTHNORTHEAST)
                return avalibleDirections.NORTHEAST;
            else if (dir == avalibleDirections.NORTHEAST)
                return avalibleDirections.EASTNORTHEAST;
            else if (dir == avalibleDirections.NORTHEAST)
                return avalibleDirections.EASTNORTHEAST;
            else if (dir == avalibleDirections.EASTNORTHEAST)
                return avalibleDirections.SOUTHEAST;
            else if (dir == avalibleDirections.SOUTHEAST)
                return avalibleDirections.SOUTHSOUTHEAST;
            else if (dir == avalibleDirections.SOUTHSOUTHEAST)
                return avalibleDirections.SOUTH;
            else if (dir == avalibleDirections.SOUTH)
                return avalibleDirections.SOUTHSOUTHWEST;
            else if (dir == avalibleDirections.SOUTHSOUTHWEST)
                return avalibleDirections.SOUTHWEST;
            else if (dir == avalibleDirections.SOUTHWEST)
                return avalibleDirections.WESTSOUTHWEST;
            else if (dir == avalibleDirections.WESTSOUTHWEST)
                return avalibleDirections.WEST;
            else if (dir == avalibleDirections.WEST)
                return avalibleDirections.NORTHWESTNORTH;
            else if (dir == avalibleDirections.NORTHWESTNORTH)
                return avalibleDirections.NORTHWEST;
            else if (dir == avalibleDirections.NORTHWEST) {
                return avalibleDirections.NORTHNORTHWEST;
            }
            return avalibleDirections.NORTH;
        }
    }
}