/**
 * Drone class will create all the drones which will be added to the canvas.
 * Drone has its own unique cooridnates and direction
 * @author Ian
 */


public class Drone extends createdrone{

    private double dx;
    private double dy;
    private DroneDirection.avalibleDirections droneDir; 
    public static int droneCount = 0;
    private int dronneID;

    /**
     * constructor for drone
     * @param x
     * @param y
     * @param dirc
     * 
     */

    Drone(float x, float y, DroneDirection.avalibleDirections dirc) {
        dx = x;
        dy = y;
        droneDir = dirc;
        dronneID = droneCount++;
    }

    public double getX() {  //getter function
        return dx;
    }

    public double getY() {  //getter funciton
        return dy;
    }

    /**
     * converting drone informtion into string format.
     */

    public String toString() {
        return "Drone "+ dronneID +" " +"is at " + Math.round(dx) + "," + Math.round(dy) + " facing the direction "
                + droneDir + ".";
    }

    /**
         * this function is resposible for the all the movements of drones.
         * if the space is occupid by any other drone the new drone wont be added to that position
         * finally drone will move in to its respective postion based according to the full 4x4 graph in 2 speeds.
     */


    public void moveDrone(DroneArena arena) {

        double xaxis = 0;
        double yaxis = 0;
        double speed1=0.5;
        double speed2=1;

        if (droneDir == DroneDirection.avalibleDirections.NORTH) {
            xaxis = dx;
            yaxis = dy + speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.NORTHNORTHEAST) {
            xaxis = dx + speed1;
            yaxis = dy + speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.EASTNORTHEAST) {
            xaxis = dx + speed2;
            yaxis = dy + speed1;
        } else if (droneDir == DroneDirection.avalibleDirections.NORTHEAST) {
            xaxis = dx + speed2;
            yaxis = dy + speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.NORTHNORTHEAST) {
            xaxis = dx - speed1;
            yaxis = dy + speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.NORTHNORTHWEST) {
            xaxis = dx - speed2;
            yaxis = dy + speed1;
        } else if (droneDir == DroneDirection.avalibleDirections.NORTHWEST) {
            xaxis = dx + speed2;
            yaxis = dy - speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.EAST) {
            xaxis = dx + speed2;
            yaxis = dy;
        } else if (droneDir == DroneDirection.avalibleDirections.EASTSOUTHEAST) {
            xaxis = dx + speed2;
            yaxis = dy - speed1;
        } else if (droneDir == DroneDirection.avalibleDirections.SOUTHSOUTHEAST) {
            xaxis = dx + speed1;
            yaxis = dy - speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.SOUTH) {
            xaxis = dx;
            yaxis = dy - speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.SOUTHEAST) {
            xaxis = dx - speed2;
            yaxis = dy + speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.WESTSOUTHWEST) {
            xaxis = dx - speed2;
            yaxis = dy - speed1;
        } else if (droneDir == DroneDirection.avalibleDirections.SOUTHSOUTHEAST) {
            xaxis = dx - speed1;
            yaxis = dy - speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.SOUTHWEST) {
            xaxis = dx - speed2;
            yaxis = dy - speed2;
        } else if (droneDir == DroneDirection.avalibleDirections.WEST) {
            xaxis = dx - speed2;
            yaxis = dy;
        }

        boolean yes = arena.avalible(xaxis, yaxis);  // this will check if the space is avalible for the drone to move.
        if (yes) {
            this.dx = xaxis;
            this.dy = yaxis;
            System.out.println(this.toString());
        } else {
            droneDir = droneDir.newDirection(droneDir);
        }
    }
}