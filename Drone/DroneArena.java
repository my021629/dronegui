/**
 * DroneArena class will be used by the DroneInterface class according to the size provided.
 * It wil also add the drone ini the areana by generating random coordinates.
 * @author Ian
 */


import java.util.*;
import javafx.scene.control.ListView;

public class DroneArena {
  private int arenaWidth; // width for areana
  private int arenaHeight;
  Random randxy;  // random class object to get random coordinates
  private int randX;
  private int randY;
  public ArrayList<Drone> droneslist;  // arraylist storing the drones.

  public DroneArena(int arenaWidth, int arenaHeight) {
    this.arenaWidth = arenaWidth;
    this.arenaHeight = arenaHeight;
    droneslist = new ArrayList<Drone>();
    randxy = new Random();
  }

  /**
   * this funtion is respoible for adding the drones at random position.
   * all drones added to the arraylist will be turned in to javafx listview.
   * @param dc
   * @param fullDrone
   */

  public void addDrone(DroneCanvas dc,ListView<Drone> fullDrones) {
    boolean check = true;
    while (check) {
      randX = randxy.nextInt(arenaWidth);
      randY = randxy.nextInt(arenaHeight);
      if (randX > 0 && randX < arenaWidth - 55 && randY > 0 && randY < arenaHeight - 55 && !occupied(randX, randY)) {
        DroneDirection.avalibleDirections randdrone = DroneDirection.avalibleDirections.randomDirections();
        droneslist.add(new Drone(randX, randY, randdrone));
        check = false;
      }
      dc.drawDrone(this);
      DroneInterface.showAll(fullDrones);
    }
  }

  /*
   * movealldroes will move all the drone on the canvas provided.
   */
  public void moveAllDrones(DroneCanvas dc){
    for(Drone d: droneslist){
        d.moveDrone(this) ;
    }
    dc.changeCanvas(this);
}

/**
 * this fucntion will check if any drone is there where drone tries to move or when drones get added..
 * @param horizontally
 * @param vertically
 * @return
 */

  public boolean occupied(double horizontally, double vertically) {
    boolean droneIsThere = false;

    for (Drone d : droneslist) {
      if (horizontally == d.getX() || horizontally + 60 == d.getX() || horizontally - 60 == d.getX()) {
        droneIsThere = true;
        break;
      }
      if (vertically == d.getY() || vertically + 60 == d.getY() || vertically - 60 == d.getY()) {
        droneIsThere = true;
        break;
      }
    }
    return droneIsThere;
  }

  /*
   * avalible checks if space is avalible for the drone to move
   */
  public boolean avalible(double x, double y) {
    return !occupied(x, y) && x > 0 && y > 0 && x < arenaWidth - 55 && y < arenaHeight - 55;
  }

}
