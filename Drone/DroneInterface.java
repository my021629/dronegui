
/**
 * DroneInterface class will be used for user to intract will the system.
 * multiple javaFx funtions have been used for animation and graphic user interface.
 * different javaFx llibraries have been imported including the button,menu and Hbox.
 */

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class DroneInterface extends Application {
    private DroneCanvas dc; // object of DroneCanvas class
    private static DroneArena mainArena; // object of DroneArena
    private static AnimationTimer animation; // animation class

    public static void main(String[] args) {
        Application.launch(args);
    }

    public static void showAll(ListView<Drone> droneM) { 
        droneM.getItems().clear();
        for (Drone d : mainArena.droneslist)
            droneM.getItems().add(d);
    }

    @Override
    public void start(Stage PrimaryStage) throws Exception {

        /* creating a MenuBar for objects for the menu objects to be mapped */
        MenuBar MainMenu = new MenuBar();
        /* now creating menu objects */
        Menu file = new Menu("File");
        Menu Help = new Menu("Help");
        Menu about = new Menu("About");
        Alert a = new Alert(AlertType.NONE);

        /* adding and mapping all the menu objects to the menubar */
        MainMenu.getMenus().add(file);
        MainMenu.getMenus().add(Help);
        MainMenu.getMenus().add(about);
        /*
         * crating the submenus using the class MenuItem which would be mapped to their
         * respective menu
         */
        /* Border pane to place the menu at the top of screen */
        MenuItem how = new MenuItem("How It Works?");
        MenuItem contactUs = new MenuItem("Contact Us");

        MenuItem aboutUs = new MenuItem("About us");
        MenuItem quit = new MenuItem("Quit");
        /* Mapping MenuItems Items to Menus */
        file.getItems().add(quit);
        Help.getItems().add(how); // adding dropdown menu option
        Help.getItems().add(contactUs);
        about.getItems().add(aboutUs);

        quit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.exit();
            }
        });
        //

        contactUs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                a.setAlertType(AlertType.INFORMATION);

                // set content text
                a.setContentText("if you need any further assistance please email my021629@student.reading.ac.uk");

                // show the dialog
                a.show();
            }
        });

        how.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                a.setAlertType(AlertType.INFORMATION);

                // set content text
                a.setContentText(
                        "Drones can be added in to the areana by pressing the 'ADD DRONE' button. for moving the drones press 'START' and 'PAUSE' to pause the movement.");

                // show the dialog
                a.show();
            }
        });

        aboutUs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                a.setAlertType(AlertType.INFORMATION);

                a.setContentText(
                        "Drone Application is designed and developed by Ian for Universty of reading's coursework");

                a.show();
            }
        });


        ListView<Drone> fullDrone = new ListView<>();
        Text text = new Text("Drones Status: ");
        text.setFill(Color.RED);
        text.setStrokeWidth(1.25);
        text.setStroke(Color.RED);

        VBox detials = new VBox(text);
        detials.setPrefWidth(400);// prefWidth
        detials.setPrefHeight(600);// prefHeigh
        detials.getChildren().addAll(fullDrone);
        detials.setAlignment(Pos.CENTER);
        detials.setPadding(new Insets(0, 0, 0, 100));

        VBox controlles = new VBox(70);
        Button start = new Button("START");
        start.setStyle(
                "-fx-background-radius: 20em; " +
                        "-fx-min-width: 200px; " +
                        "-fx-min-height: 200px; " +
                        "-fx-max-width: 200px; " +
                        "-fx-max-height: 200px;" +
                        "-fx-background-color: green;");

        start.setOnAction(e -> animation.start());

        Button pause = new Button("PAUSE");
        pause.setStyle(
                "-fx-background-radius: 20em; " +
                        "-fx-min-width: 200px; " +
                        "-fx-min-height: 200px; " +
                        "-fx-max-width: 200px; " +
                        "-fx-max-height: 200px;" +
                        "-fx-background-color: red;");
        pause.setOnAction(e -> animation.stop());
        Button addDrone = new Button("ADD DRONES");
        addDrone.setStyle(
                "-fx-background-radius: 20em; " +
                        "-fx-min-width: 200px; " +
                        "-fx-min-height: 200px; " +
                        "-fx-max-width: 200px; " +
                        "-fx-max-height: 200px;" +
                        "-fx-background-color:  #FFD700;");
        addDrone.setOnAction(e -> {
            mainArena.addDrone(dc, fullDrone);
        });

        controlles.getChildren().addAll(addDrone, start, pause);

        Group group = new Group();
        Canvas c = new Canvas();
        c.setWidth(850);
        c.setHeight(650);
        group.getChildren().add(c);
        dc = new DroneCanvas(c.getGraphicsContext2D(), 850, 650);
        dc.setFillArenaColour(850, 650);

        Text welcome = new Text();
        welcome.setText("Welcome to Drone Arena");
        welcome.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 50));
        welcome.setFill(Color.BROWN);
        welcome.setStrokeWidth(2);
        welcome.setX(75);
        welcome.setY(-30);
        welcome.setStroke(Color.BLUE);

        welcome.setTextOrigin(VPos.CENTER);

        group.getChildren().add(welcome);
        mainArena = new DroneArena(850, 650);
        dc.drawDrone(mainArena);
        // dc.drawjetDrone(mainArena);
        group.getChildren().add(MainMenu);

        animation = new AnimationTimer() {
            public void handle(long now) {
                mainArena.moveAllDrones(dc);
            }
        };
        /*
         * borderpane will pass all the layout to the stage to be shown.
         */
        BorderPane BP = new BorderPane();

        BP.setTop(MainMenu); // passing the menu to the borderPane to be put on top.
        BP.setLeft(controlles);
        BP.setCenter(group);
        BP.setRight(detials);

        Scene sc = new Scene(BP);
        PrimaryStage.setScene(sc);
        PrimaryStage.setWidth(1920);
        PrimaryStage.setHeight(1080);
        PrimaryStage.setTitle("Drone Application");
        PrimaryStage.show();

    }

}